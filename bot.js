var Discord = require('discord.js');
var logger = require('winston');
var auth = require('./auth.json');
const http = require('http');
const https = require('https');
var qs = require('querystring');
const _ = require('lodash');
var pokedex = require('./pokedex.json');

var posted =[];
var gyms = [
	'48.819115002.23868800', //Meudon-sur-Seine Tramway
	...
];

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
// Initialize Discord Bot
const bot = new Discord.Client();
const HOSTNAME = '...';
const PORT = ...;

const server = http.createServer((request, res) => {
	
	if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
			var pok = JSON.parse(qs.parse(body).data);
			let canal = bot.channels.get('588379546861043713'); //test
			canal.send(pok.name + ' ' + pok.sex + ', iv : ' + pok.iv + ', cp : ' + pok.cp + ', lvl : ' + pok.lvl).catch(console.error);
			canal.send(pok.where).catch(console.error);
        });
    }
	
	res.statusCode = 200;
	res.setHeader('Content-Type', 'text/plain');
	res.end('Hello dev.to!\n');
});

server.listen(PORT, HOSTNAME, () => {
	console.log('Server running at ' + HOSTNAME + ' on port ' + PORT);
});

bot.on('ready', function (evt) {
    logger.info('Connected');
	setInterval(()=>getRaids(),60000);
});
bot.on('message', function (message) {
    if (message.content === '!ping') {
		message.reply('pong');
    }
	if (_.startsWith(message.content, '!calc')) {
		var msg = _.split(message.content, ' ');
		var km = caldis(msg[1],msg[2]);
		var time = disTime(km);
		message.reply('km : ' + km + ', time : ' + time);
    }
});

bot.on("guildMemberAdd", (member) => {
	let canal = bot.channels.get('381021639812317187'); //bla bla
	canal.send('Bonjour, '+ member.user.username +' Bienvenue dans le coin du casu ! :)').catch(console.error);
	canal.send('Choose your color wisely !').then((msg) => {
		msg.react('557359772895019008')
		.then(() => msg.react('557360498547490836'))
		.then(() => msg.react('557360499910639617'))
		.catch(() => console.error('One of the emojis failed to react.'));
	});
});

bot.on('messageReactionAdd', (reaction, user) => {
	if(reaction.message.author.username==='meudonSeineBot'){
		let guild = bot.guilds.get('381021639359463424'); //server
		let role = guild.roles.get('585397799236141056'); //casu
		let roleM = guild.roles.get('585430021716443136'); //magicians
		let valor = guild.roles.get('453958164426129408'); //valor
		let mystic = guild.roles.get('453957002721230878'); //mystic
		let instinct = guild.roles.get('453956436226080781'); //instinct
		//390528937618571266
		guild.fetchMember(user).then(member => {
			if(reaction.emoji.name === 'spinda') {
					member.addRole(role);
			} else if(reaction.emoji.name === 'Psychic') {
					member.removeRole(role);
			} else if(reaction.emoji.name === 'stardust') {
					member.addRole(roleM);
			} else if(reaction.emoji.name === 'Poison') {
					member.removeRole(roleM);
			} else if(reaction.emoji.name === 'valor') {
					member.addRole(valor);
					member.removeRole(mystic);
					member.removeRole(instinct);
			} else if(reaction.emoji.name === 'mystic') {
					member.addRole(mystic);
					member.removeRole(valor);
					member.removeRole(instinct);
			} else if(reaction.emoji.name === 'instinct') {
					member.addRole(instinct);
					member.removeRole(valor);
					member.removeRole(mystic);
			}
		});
	}
});

bot.on('raw', packet => {
    // We don't want this to run on unrelated packets
    if (!['MESSAGE_REACTION_ADD', 'MESSAGE_REACTION_REMOVE'].includes(packet.t)) return;
    // Grab the channel to check the message from
    const channel = bot.channels.get(packet.d.channel_id);
    // There's no need to emit if the message is cached, because the event will fire anyway for that
    if (channel.messages.has(packet.d.message_id)) return;
    // Since we have confirmed the message is not cached, let's fetch it
    channel.fetchMessage(packet.d.message_id).then(message => {
        // Emojis can have identifiers of name:id format, so we have to account for that case as well
        const emoji = packet.d.emoji.id ? `${packet.d.emoji.name}:${packet.d.emoji.id}` : packet.d.emoji.name;
        // This gives us the reaction we need to emit the event properly, in top of the message object
        const reaction = message.reactions.get(emoji);
        // Adds the currently reacting user to the reaction's users collection.
        if (reaction) reaction.users.set(packet.d.user_id, bot.users.get(packet.d.user_id));
        // Check which type of event it is before emitting
        if (packet.t === 'MESSAGE_REACTION_ADD') {
            bot.emit('messageReactionAdd', reaction, bot.users.get(packet.d.user_id));
        }
        if (packet.t === 'MESSAGE_REACTION_REMOVE') {
            bot.emit('messageReactionRemove', reaction, bot.users.get(packet.d.user_id));
        }
    });
});


function round(number,X) {
	X = (!X ? 3 : X);
	return Math.round(number*Math.pow(10,X))/Math.pow(10,X);
}
function caldis(coord1, coord2) {
	var lat1 = Number(_.split(coord1,',')[0]);
	var long1 = Number(_.split(coord1,',')[1]);;
	var lat2 = Number(_.split(coord2,',')[0]);;
	var long2 = Number(_.split(coord2,',')[1]);;
	var lat1r = (3.14159265358979*lat1/180);
	var long1r = (3.14159265358979*long1/180);
	var lat2r = (3.14159265358979*lat2/180);
	var long2r = (3.14159265358979*long2/180);
	var coco = (Math.cos(lat1r)*Math.cos(lat2r)*Math.cos(long1r)*Math.cos(long2r)+Math.cos(lat1r)*Math.sin(long1r)*Math.cos(lat2r)*Math.sin(long2r)+Math.sin(lat1r)*Math.sin(lat2r)); 
	var flat = (Math.acos(coco));
	return round(6371*flat);
}

function disTime(km){
	if(km>1500){return 120;}
	if(km>99){return Math.floor((85/1400)*km+29.9);}
	if(km>22){return Math.floor((15/60)*km+10);}
	return Math.round((20/30)*km);
}

async function getRaids () {
	https.get('https://pogoRaids.com', (res) => {
		const { statusCode } = res;
		const contentType = res.headers['content-type'];
		res.setEncoding('utf8');
		let rawData = '';
		res.on('data', (chunk) => { rawData += chunk; });
		res.on('end', () => {
			let data = JSON.parse(rawData);
			
			for (var i = 0; i < data.length; i++) {
				var raid = data[i];
				var name = raid.gym_name;
				var start = raid.start_time.substring(11,16);
				var end = raid.end_time.substring(11,16);

				if(_.includes(gyms, raid.lat+raid.lon) && raid.raid_level==='5' && !_.includes(posted, raid.id))
				{
					//let canal = bot.channels.get('588379546861043713'); //test
					let canal = bot.channels.get('585042571722489856'); //inter-ville
					let notify = (raid.lat+raid.lon==='48.819115002.23868800') ? ' <@300590271459098629> ':''; 
					let msg = start + ' -> ' + end + ' :     **' + name + '** ' +  notify ;
					
					let acronym = name.split(' ').reduce((r,w)=> r+=w.slice(0,1),'');
					let pok = raid.pokemon==='9995' ? '5' : pokedex[(raid.pokemon*1)-1].name.french;
					msg += '   (!r ' + pok + ' ' + _.toLower(acronym) + ' ' + start + ')';
					
					send(canal, msg);
					
					posted.push(raid.id);
					setTimeout(()=>posted.shift(),7200000);
				}
			}
		});
	}).on('error', (e) => {
		console.error('Got error: ' + e.message);
	});
}
async function send (canal, msg) {
	await canal.send(msg).catch(console.error);
}

bot.login(auth.token);